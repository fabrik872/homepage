<?php


namespace App\Services;


use App\Entity\Links;
use JonathanKowalski\Omg\Parser;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class OpenGraphExtractor
{
    private $url;
    private $html;
    private $imageSaver;
    private $encodedFile;
    private $flashBag;

    public function __construct(ImageSaver $imageSaver, UploadedBase64EncodedFile $encodedFile, FlashBagInterface $flashBag)
    {
        $this->imageSaver = $imageSaver;
        $this->encodedFile = $encodedFile;
        $this->flashBag = $flashBag;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
        $this->html = file_get_contents($url, false, $this->timeout());
    }

    public function getUrlBase()
    {
        return parse_url($this->url)['host'];
    }

    public function getOgTags()
    {
        $pattern = '/<\s*meta\s+property="og:([^"]+)"\s+content="([^"]*)/i';
        $patternColor = '/<\s*meta\s+name="([^"]+)"\s+content="([^"]*)/i';
        if (preg_match_all($pattern, $this->html, $out)) {
            if (
                preg_match_all($patternColor, $this->html, $outColor) &&
                isset($outColor[2][2]) &&
                ctype_xdigit(substr($outColor[2][2], 1)) &&
                strlen($outColor[2][2]) == 7
            )
                return array_merge(array_combine($out[1], $out[2]), ['theme-color' => $outColor[2][2]]);
            return array_combine($out[1], $out[2]);
        }
        return array();
    }

    public function fillLink(Links $links): Links
    {
        $og = $this->getOgTags();
        if (isset($og['title']))
            $links->setName($og['title']);
        elseif (isset($og['site_name']))
            $links->setName($og['site_name']);

        if (isset($og['theme-color']))
            $links->setColor($og['theme-color']);

        if (isset($og['image'])) {
            try {
                if ($this->checkUrl($this->getUrlBase() . $og['image'])) {
                    $image = $this->getUrlBase() . $og['image'];
                } elseif ($this->checkUrl($og['image'])) {
                    $image = $og['image'];
                }

                if (file_get_contents($this->addhttp($image), false, $this->timeout())) {
                    $this->imageSaver->saveImage($this->encodedFile->getUploadedFile(base64_encode(file_get_contents($this->addhttp($image))), basename($image)));
                    $links->setImageId($this->imageSaver->getImage());
                }
            } catch (\Exception $e){
                $this->flashBag->add('warning', "Cannot setup image ". $e->getMessage());
            }
        }

        return $links;
    }

    public function addhttp($url)
    {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }
        return $url;
    }

    public function checkUrl($url)
    {
        $headers = @get_headers($url);
        return is_array($headers) ? true : false;
    }

    public function timeout()
    {
        return stream_context_create(
            array('http'=>
                array(
                    'timeout' => 10,
                )
            )
        );
    }
}