<?php


namespace App\Services;


use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Mime\Encoder\Base64ContentEncoder;

class UploadedBase64EncodedFile
{
    public function getUploadedFile(string $base64String, $originalName, $mimeType = null): UploadedFile
    {
        $tmpAddress = sys_get_temp_dir() . DIRECTORY_SEPARATOR . uniqid();
        $file = fopen($tmpAddress, "wb");
        fwrite($file, base64_decode($base64String));
        fclose($file);

        return new UploadedFile($tmpAddress, $originalName, $mimeType, filesize($tmpAddress), null, true);
    }
}