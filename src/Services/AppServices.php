<?php


namespace App\Services;


use App\Entity\Images;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AppServices
{
    private $bag;

    public function __construct(ParameterBagInterface $bag)
    {
        $this->bag = $bag;
    }

    public function folderSize(string $dir)
    {
        $size = 0;
        foreach (glob(rtrim($dir, '/') . '/*', GLOB_NOSORT) as $each) {
            $size += is_file($each) ? filesize($each) : $this->folderSize($each);
        }
        return $size;
    }

    function rmdir(string $src)
    {
        $dir = opendir($src);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                $full = $src . '/' . $file;
                if (is_dir($full)) {
                    $this->rmdir($full);
                } else {
                    unlink($full);
                }
            }
        }
        closedir($dir);
        rmdir($src);
    }

    public function getVersion()
    {
        $commitMessage = trim(exec("git log -1 --pretty=format:'%h - %cn (%ce) - %s (%ci)'"));

        return sprintf('%s', $commitMessage);
    }

    public function imageLink(?string $image)
    {
        if($image == null){
            $package = new Package(new EmptyVersionStrategy());

            return $package->getUrl('assets/img/Website-PNG-Transparent.png');
        }
        return $this->bag->get("uploadFolder") . '/' . $image;
    }
}