<?php


namespace App\Services;


use App\Entity\Images;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class ImageSaver
{
    private $param;
    private $image;
    private $flash;
    private $em;
    private $uploadFolder;

    public function __construct(ParameterBagInterface $parameterBag, FlashBagInterface $flashBag, EntityManagerInterface $em)
    {
        $this->param = $parameterBag;
        $this->flash = $flashBag;
        $this->em = $em;
        $this->uploadFolder = $this->param->get('kernel.project_dir') . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . $this->param->get('uploadFolder');
    }

    public function saveImage(UploadedFile $uploadedFile): void
    {
        $fileName = md5(base64_encode(fread(fopen($uploadedFile->getRealPath(), 'r'), filesize($uploadedFile->getRealPath()))));

        $this->image = new Images();
        $this->image->setFileName($fileName . '.' . $uploadedFile->guessExtension());
        $this->image->setOriginalName($uploadedFile->getClientOriginalName());
        $this->imageId = $this->image;
        $uploadedFile->move($this->uploadFolder, $fileName . '.' . $uploadedFile->guessExtension());
    }

    public function loadImage(Images $images): void
    {
        $this->image = $images;
    }

    public function delete(): bool
    {
        if ($this->em->getRepository(Images::class)->getImageUses($this->image) <= 1) {
            unlink($this->uploadFolder . DIRECTORY_SEPARATOR . $this->image->getFileName());
        }

        $this->em->remove($this->image);
        $this->em->flush();

        return true;
    }

    public function getImage(): ?Images
    {
        return $this->image;
    }
}
