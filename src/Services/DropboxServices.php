<?php


namespace App\Services;


use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use League\Flysystem\Filesystem;
use Srmklive\Dropbox\Adapter\DropboxAdapter;
use Srmklive\Dropbox\Client\DropboxClient;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class DropboxServices
{
    /** @var $client DropboxClient */
    private $client;
    /** @var $adapter DropboxAdapter */
    private $adapter;
    /** @var $filesystem Filesystem */
    private $filesystem;
    private $flashBag;
    private $em;
    /** @var $user User */
    private $user;
    /** @var $kernel KernelInterface */
    private $kernel;

    public function __construct(FlashBagInterface $flashBag, EntityManagerInterface $em, KernelInterface $kernel)
    {
        $this->flashBag = $flashBag;
        $this->em = $em;
        $this->kernel = $kernel;
    }

    public function startDropbox(User $user)
    {
        $this->client = new DropboxClient($user->getDropboxToken());

        $this->adapter = new DropboxAdapter($this->client);

        $this->filesystem = new Filesystem($this->adapter);

        $this->user = $user;

        try {
            $this->client->listFolder('');
        } catch (\Exception $e) {
            $this->flashBag->add('danger', 'Exception: ' . $e->getMessage());
            return false;
        }
        return true;
    }

    public function getFilesystem(): ?Filesystem
    {
        return $this->filesystem;
    }

    public function getBackups()
    {
        $files = $this->client->listFolder('');
        $backups = [];
        foreach ($files['entries'] as $file) {
            $backups[] = $file['name'];
        }

        return $backups;
    }

    public function push()
    {
        $this->getFilesystem()->put($this->user->getBackupName() . '/var/data.db', file_get_contents($this->kernel->getProjectDir() . '/var/data.db'));
        $images = scandir($this->kernel->getProjectDir() . '/public/uploads/');
        $this->getFilesystem()->createDir($this->user->getBackupName() . '/public/uploads/');
        $imagesOnServer = $this->client->listFolder($this->user->getBackupName() . '/public/uploads/')['entries'];
        $imagesOnServerSimple = [];
        $removed = 0;
        $uploaded = 0;
        foreach ($imagesOnServer as $imageOnServer) {
            if (!in_array($imageOnServer['name'], $images)) {
                $this->getFilesystem()->delete($imageOnServer['path_display']);
                $removed++;
            }
            $imagesOnServerSimple[] = $imageOnServer['name'];
        }
        foreach ($images as $image) {
            if (!in_array($image, ['.', '..']) && !in_array($image, $imagesOnServerSimple)) {
                $this->getFilesystem()->write($this->user->getBackupName() . '/public/uploads/' . $image, file_get_contents($this->kernel->getProjectDir() . '/public/uploads/' . $image));
                $uploaded++;
            }
        }
        return [
            'removed' => $removed,
            'uploaded' => $uploaded
        ];
    }

    public function pull()
    {
        file_put_contents(
            $this->kernel->getProjectDir() . '/var/data.db',
            $this->getFilesystem()->read($this->user->getBackupName() . '/var/data.db')
        );
        $images = scandir($this->kernel->getProjectDir() . '/public/uploads/');
        $imagesOnServer = $this->client->listFolder($this->user->getBackupName() . '/public/uploads/')['entries'];

        $imagesOnServerSimple = [];
        $removed = 0;
        $downloaded = 0;
        if (!file_exists($this->kernel->getProjectDir() . '/public/uploads/')) {
            mkdir($this->kernel->getProjectDir() . '/public/uploads/');
        }
        foreach ($imagesOnServer as $imageOnServer) {
            if (!in_array($imageOnServer['name'], $images)) {
                file_put_contents(
                    $this->kernel->getProjectDir() . '/public/uploads/' . $imageOnServer['name'],
                    $this->getFilesystem()->read($imageOnServer['path_display'])
                );
                $downloaded++;
            }
            $imagesOnServerSimple[] = $imageOnServer['name'];
        }
        foreach ($images as $image) {
            if (!in_array($image, ['.', '..']) && !in_array($image, $imagesOnServerSimple)) {
                unlink($this->kernel->getProjectDir() . '/public/uploads/' . $image);
                $removed++;
            }
        }
        return [
            'removed' => $removed,
            'downloaded' => $downloaded
        ];
    }

    public function autoPull(UserInterface $user)
    {
        if ($user->getAutosync()) {
            try {
                $this->startDropbox($user);
                $report = $this->pull();
                $this->flashBag->add('success', $report['downloaded'] . ' files downloaded and ' . $report['removed'] . ' removed');
            } catch (\Exception $e) {
                $this->flashBag->add('danger', 'Error: ' . $e->getMessage());
            }
        }
    }

    public function autoPush(UserInterface $user)
    {
        if ($user->getAutosync()) {
            try {
                $this->startDropbox($user);
                $report = $this->push();
                $this->flashBag->add('success', $report['uploaded'] . ' files uploaded and ' . $report['removed'] . ' removed');
            } catch (\Exception $e) {
                $this->flashBag->add('danger', 'Error: ' . $e->getMessage());
            }
        }
    }
}