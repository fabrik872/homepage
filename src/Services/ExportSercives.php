<?php

namespace App\Services;

use App\Entity\Links;
use App\Repository\LinksRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class ExportSercives
{
    private $em;
    private $linksRepo;
    private $imageSaver;
    private $encodedFile;
    private $order;
    private $appServices;

    public function __construct(
        EntityManagerInterface $em,
        LinksRepository $linksRepo,
        ImageSaver $imageSaver,
        UploadedBase64EncodedFile $encodedFile,
        AppServices $appServices
    )
    {
        $this->em = $em;
        $this->linksRepo = $linksRepo;
        $this->imageSaver = $imageSaver;
        $this->encodedFile = $encodedFile;
        $this->appServices = $appServices;
    }

    public function importLink(\stdClass $item, UserInterface $user): Links
    {
        $this->linksRepo->getLast($user);
        $this->order = $this->order ?: $this->linksRepo->getLast($user);

        $link = new Links();
        $link->setXOrder($this->order++);
        $link->setUserId($user);
        $link->setName($item->links->name);
        $link->setUrl($item->links->url);
        $link->setXHidden(isset($item->links->hidden) ? $item->links->hidden : false);
        $link->setXPrivate(false);
        $link->setColor(isset($item->links->color) ? $item->links->color : "#000000");
        $link->setDate(new \DateTime($item->links->date->date));
        if ($item->imageFile) {
            $this->imageSaver->saveImage($this->encodedFile->getUploadedFile($item->imageFile, $item->images->originalName));
            $link->setImageId($this->imageSaver->getImage());
        }
        return $link;
    }


    public function exportLink(Links $links): \stdClass
    {
        $output = new \stdClass();
        $link = new \stdClass();
        $image = new \stdClass();

        $link->name = $links->getName();
        $link->url = $links->getUrl();
        $link->color = $links->getColor();
        $link->hidden = $links->getXHidden();
        $link->date = $links->getDate();
        $file = '';
        if ($links->getImageId()) {
            $image->originalName = $links->getImageId()->getOriginalName();
            $image->fileName = $links->getImageId()->getFileName();
            $image->date = $links->getImageId()->getDate();
            $fileLocation = $this->appServices->imageLink($links->getImageId());
            $file = base64_encode(fread(fopen($fileLocation, 'r'), filesize($fileLocation)));
        }

        $output->links = $link;
        $output->images = $image;
        $output->imageFile = $file;
        return $output;
    }
}