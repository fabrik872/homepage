<?php

namespace App\Twig;

use App\Entity\Images;
use App\Entity\User;
use App\Entity\Wallpaper;
use App\Services\AppServices;
use App\Services\randomWallpaper;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    private $em;
    private $appServices;

    public function __construct(EntityManagerInterface $em, AppServices $appServices)
    {
        $this->em = $em;
        $this->appServices = $appServices;
    }

    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('bytes', [$this, 'readableBytes']),
            new TwigFilter('imageLink', [$this, 'imageLink']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('randomWallpaper', [$this, 'randomWallpaper']),
        ];
    }

    public function randomWallpaper(User $user)
    {
        return $this->em->getRepository(Wallpaper::class)->getRandomWallpaper($user);
    }

    public function readableBytes($bytes)
    {
        if ($bytes) {
            $i = floor(log($bytes) / log(1024));
        } else {
            return "0 B";
        }
        $sizes = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');

        return sprintf('%.02F', $bytes / pow(1024, $i)) * 1 . ' ' . $sizes[$i];
    }

    public function imageLink(?Images $image)
    {
        return $this->appServices->imageLink($image);
    }
}
