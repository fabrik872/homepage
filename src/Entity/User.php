<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Links", mappedBy="userId")
     */
    private $links;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Wallpaper", mappedBy="userId", orphanRemoval=true)
     */
    private $wallpapers;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dropboxToken;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $backupName;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Images", inversedBy="yes", cascade={"persist", "remove"})
     */
    private $profilePicture;

    /**
     * @ORM\Column(type="boolean")
     */
    private $autosync;

    public function __construct()
    {
        $this->links = new ArrayCollection();
        $this->wallpapers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|Links[]
     */
    public function getLinks(): Collection
    {
        return $this->links;
    }

    public function addLink(Links $link): self
    {
        if (!$this->links->contains($link)) {
            $this->links[] = $link;
            $link->setUserId($this);
        }

        return $this;
    }

    public function removeLink(Links $link): self
    {
        if ($this->links->contains($link)) {
            $this->links->removeElement($link);
            // set the owning side to null (unless already changed)
            if ($link->getUserId() === $this) {
                $link->setUserId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Wallpaper[]
     */
    public function getWallpapers(): Collection
    {
        return $this->wallpapers;
    }

    public function addWallpaper(Wallpaper $wallpaper): self
    {
        if (!$this->wallpapers->contains($wallpaper)) {
            $this->wallpapers[] = $wallpaper;
            $wallpaper->setUserId($this);
        }

        return $this;
    }

    public function removeWallpaper(Wallpaper $wallpaper): self
    {
        if ($this->wallpapers->contains($wallpaper)) {
            $this->wallpapers->removeElement($wallpaper);
            // set the owning side to null (unless already changed)
            if ($wallpaper->getUserId() === $this) {
                $wallpaper->setUserId(null);
            }
        }

        return $this;
    }

    public function getDropboxToken(): ?string
    {
        return $this->dropboxToken;
    }

    public function setDropboxToken(?string $dropboxToken): self
    {
        $this->dropboxToken = $dropboxToken;

        return $this;
    }

    public function getBackupName(): ?string
    {
        return $this->backupName;
    }

    public function setBackupName(?string $backupName): self
    {
        $this->backupName = $backupName;

        return $this;
    }

    public function getProfilePicture(): ?Images
    {
        return $this->profilePicture;
    }

    public function setProfilePicture(?Images $profilePicture): self
    {
        $this->profilePicture = $profilePicture;

        return $this;
    }

    public function getAutosync(): ?bool
    {
        return $this->autosync;
    }

    public function setAutosync(bool $autosync): self
    {
        $this->autosync = $autosync;

        return $this;
    }
}
