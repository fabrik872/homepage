<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImagesRepository")
 */
class Images
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $originalName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fileName;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Links", mappedBy="imageId", cascade={"persist", "remove"})
     */
    private $links;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Wallpaper", mappedBy="imageId", cascade={"persist", "remove"})
     */
    private $wallpaper;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", mappedBy="profilePicture", cascade={"persist", "remove"})
     */
    private $yes;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    public function __toString()
    {
        return $this->fileName;
    }

    public function __clone() {
        $this->id = null;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOriginalName(): ?string
    {
        return $this->originalName;
    }

    public function setOriginalName(string $originalName): self
    {
        $this->originalName = $originalName;

        return $this;
    }

    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    public function setFileName(string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getLinks(): ?Links
    {
        return $this->links;
    }

    public function setLinks(?Links $links): self
    {
        $this->links = $links;

        // set (or unset) the owning side of the relation if necessary
        $newImageId = $links === null ? null : $this;
        if ($newImageId !== $links->getImageId()) {
            $links->setImageId($newImageId);
        }

        return $this;
    }

    public function getWallpaper(): ?Wallpaper
    {
        return $this->wallpaper;
    }

    public function setWallpaper(Wallpaper $wallpaper): self
    {
        $this->wallpaper = $wallpaper;

        // set the owning side of the relation if necessary
        if ($this !== $wallpaper->getImageId()) {
            $wallpaper->setImageId($this);
        }

        return $this;
    }

    public function getYes(): ?User
    {
        return $this->yes;
    }

    public function setYes(?User $yes): self
    {
        $this->yes = $yes;

        // set (or unset) the owning side of the relation if necessary
        $newProfilePicture = $yes === null ? null : $this;
        if ($newProfilePicture !== $yes->getProfilePicture()) {
            $yes->setProfilePicture($newProfilePicture);
        }

        return $this;
    }
}
