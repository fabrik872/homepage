<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WallpaperRepository")
 */
class Wallpaper
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Images", inversedBy="wallpaper", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $imageId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="wallpapers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImageId(): ?Images
    {
        return $this->imageId;
    }

    public function setImageId(Images $imageId): self
    {
        $this->imageId = $imageId;

        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->userId;
    }

    public function setUserId(?User $userId): self
    {
        $this->userId = $userId;

        return $this;
    }
}
