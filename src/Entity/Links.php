<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LinksRepository")
 */
class Links
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $url;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Images", inversedBy="links", cascade={"persist", "remove"})
     */
    private $imageId;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Keywords", inversedBy="links")
     */
    private $keywordsId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="links")
     */
    private $userId;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     */
    private $xOrder;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LinksLog", mappedBy="linksId", orphanRemoval=true)
     */
    private $linksLogs;

    /**
     * @ORM\Column(type="string", length=8, nullable=true)
     */
    private $color;

    /**
     * @ORM\Column(type="boolean")
     */
    private $xPrivate;

    /**
     * @ORM\Column(type="boolean")
     */
    private $xHidden;

    private $data;

    public function __construct()
    {
        $this->keywordsId = new ArrayCollection();
        $this->date = new \DateTime();
        $this->linksLogs = new ArrayCollection();
    }

    public function __clone() {
        $this->id = null;
    }

    public function __get($varName){

        if (!array_key_exists($varName,$this->data)){
            //this attribute is not defined!
            throw new \Exception($varName . ' not exist');
        }
        else return $this->data[$varName];

    }

    public function __set($varName,$value){
        $this->data[$varName] = $value;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getImageId(): ?Images
    {
        return $this->imageId;
    }

    public function setImageId(?Images $imageId): self
    {
        $this->imageId = $imageId;

        return $this;
    }

    /**
     * @return Collection|Keywords[]
     */
    public function getKeywordsId(): Collection
    {
        return $this->keywordsId;
    }

    public function addKeywordsId(Keywords $keywordsId): self
    {
        if (!$this->keywordsId->contains($keywordsId)) {
            $this->keywordsId[] = $keywordsId;
        }

        return $this;
    }

    public function removeKeywordsId(Keywords $keywordsId): self
    {
        if ($this->keywordsId->contains($keywordsId)) {
            $this->keywordsId->removeElement($keywordsId);
        }

        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->userId;
    }

    public function setUserId(?User $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getXOrder(): ?int
    {
        return $this->xOrder;
    }

    public function setXOrder(int $xOrder): self
    {
        $this->xOrder = $xOrder;

        return $this;
    }

    /**
     * @return Collection|LinksLog[]
     */
    public function getLinksLogs(): Collection
    {
        return $this->linksLogs;
    }

    public function addLinksLog(LinksLog $linksLog): self
    {
        if (!$this->linksLogs->contains($linksLog)) {
            $this->linksLogs[] = $linksLog;
            $linksLog->setLinksId($this);
        }

        return $this;
    }

    public function removeLinksLog(LinksLog $linksLog): self
    {
        if ($this->linksLogs->contains($linksLog)) {
            $this->linksLogs->removeElement($linksLog);
            // set the owning side to null (unless already changed)
            if ($linksLog->getLinksId() === $this) {
                $linksLog->setLinksId(null);
            }
        }

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getXPrivate(): ?bool
    {
        return $this->xPrivate;
    }

    public function setXPrivate(bool $xPrivate): self
    {
        $this->xPrivate = $xPrivate;

        return $this;
    }

    public function getXHidden(): ?bool
    {
        return $this->xHidden;
    }

    public function setXHidden(bool $xHidden): self
    {
        $this->xHidden = $xHidden;

        return $this;
    }
}
