<?php

namespace App\Repository;

use App\Entity\LinksLog;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use DoctrineExtensions\Query\Sqlite;

/**
 * @method LinksLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method LinksLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method LinksLog[]    findAll()
 * @method LinksLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LinksLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LinksLog::class);
    }

    public function getAllSitesAllTime(User $user, int $month = 0): ?array
    {
        $entityManager = $this->getEntityManager();

        $time = new \DateTime('now +' . $month . ' month');
        $query = $entityManager->createQuery(
            'SELECT count(log) as usages, log.date as date, DAY(log.date) as day
            FROM App\Entity\Links links
            JOIN App\Entity\LinksLog log
            WHERE links.id = log.linksId
            AND links.userId = :user
            AND MONTH(log.date) = MONTH(:time)
            AND YEAR(log.date) = YEAR(:time)
            GROUP BY day'
        );
        $query->setParameters([
            'user' => $user,
            'time' => $time,
        ]);

        return $query->getResult();
    }

    public function countAllSitesUsages(User $user): ?int
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT count(log)
            FROM App\Entity\Links links
            JOIN App\Entity\LinksLog log
            WHERE links.id = log.linksId
            AND links.userId = :user'
        );
        $query->setParameters([
            'user' => $user,
        ]);

        return $query->getSingleScalarResult();
    }

    public function getTopSites(User $user): ?array
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT count(log) as usages, links as link
            FROM App\Entity\Links links
            JOIN App\Entity\LinksLog log
            WHERE links.id = log.linksId
            AND links.userId = :user
            GROUP BY log.linksId
            ORDER BY usages DESC'
        );
        $query->setMaxResults(10);
        $query->setParameters([
            'user' => $user,
        ]);

        return $query->getResult();
    }

    public function deleteAll(User $user): void
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT log
            FROM App\Entity\Links links
            JOIN App\Entity\LinksLog log
            WHERE links.id = log.linksId
            AND links.userId = :user'
        );
        $query->setParameters([
            'user' => $user,
        ]);

        $linkLogs = $query->getResult();
        $this->createQueryBuilder('links_log')
            ->where('links_log.id in (:linkLogs)')
            ->setParameter('linkLogs', $linkLogs)
            ->delete()
            ->getQuery()
            ->execute();
    }

    // /**
    //  * @return LinksLog[] Returns an array of LinksLog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LinksLog
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
