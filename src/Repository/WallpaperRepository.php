<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\Wallpaper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Wallpaper|null find($id, $lockMode = null, $lockVersion = null)
 * @method Wallpaper|null findOneBy(array $criteria, array $orderBy = null)
 * @method Wallpaper[]    findAll()
 * @method Wallpaper[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WallpaperRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Wallpaper::class);
    }

    public function getRandomWallpaper(User $user): ?Wallpaper
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT w
            FROM App\Entity\Wallpaper w 
            WHERE w.userId = :user'
        );
        $query->setParameters([
            'user' => $user,
        ]);

        $wallpapers = $query->getResult();

        if ($wallpapers != null) {
            return $wallpapers[array_rand($wallpapers)];
        }
        return null;
    }

    // /**
    //  * @return Wallpaper[] Returns an array of Wallpaper objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Wallpaper
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
