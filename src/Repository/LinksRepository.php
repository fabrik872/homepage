<?php

namespace App\Repository;

use App\Entity\Links;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Gregwar\Image\Image;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * @method Links|null find($id, $lockMode = null, $lockVersion = null)
 * @method Links|null findOneBy(array $criteria, array $orderBy = null)
 * @method Links[]    findAll()
 * @method Links[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LinksRepository extends ServiceEntityRepository
{
    private $parameters;
    private $router;
    private $totalFiltered;
    private $arrayResults;

    public function __construct(ManagerRegistry $registry, ParameterBagInterface $parameters, RouterInterface $router)
    {
        parent::__construct($registry, Links::class);
        $this->parameters = $parameters;
        $this->router = $router;
    }

    /**
     * @return Links[] Returns an array of Restaurant objects
     */
    public function getLinksByUser(User $user, ?string $sort = 'l.xOrder', ?string $dir = 'ASC', ?bool $hidden = false)
    {
        if ($sort === null) {
            $sort = 'l.xOrder';
        }
        if ($dir === null) {
            $dir = 'ASC';
        }
        if ($hidden === null) {
            $hidden = false;
        }
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT l as data, count(ll) as usages
            FROM App\Entity\Links l 
            LEFT JOIN l.linksLogs ll
            WHERE l.userId = :user
            AND (l.xHidden = :hidden OR l.xHidden = 0 OR l.xHidden IS NULL)
            GROUP BY l
            ORDER BY ' . $sort . ' ' . $dir
        );
        $query->setParameters([
            'user' => $user,
            'hidden' => $hidden,
        ]);

        $result = [];

        foreach ($query->getResult() as $key => $item) {
            $result[$key] = $item['data'];
            $result[$key]->usages = $item['usages'];
        }

        return $result;
    }

    /**
     * @return int
     */
    public function getLast(User $user): int
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT l.xOrder
            FROM App\Entity\Links l 
            WHERE l.userId = :user
            ORDER BY l.xOrder DESC '
        );
        $query->setMaxResults(1);

        $query->setParameters([
            'user' => $user,
        ]);

        $result = null;
        if ($query->getOneOrNullResult() != null) {
            $result = $query->getOneOrNullResult()['xOrder'];
        }

        if ($result == null) {
            return 0;
        } else {
            return $result + 1;
        }
    }

    public function select(
        int $start,
        int $lenght,
        string $column,
        string $type,
        array $columns,
        string $search = null
    ) {
        $entityManager = $this->getEntityManager();

        $search = "%" . $search . "%";

        $query = $entityManager->createQuery(
            'SELECT count(l)
            FROM App\Entity\Links l
            JOIN l.imageId i
            WHERE l.xPrivate = 0
            AND (l.name LIKE :search
            OR l.url LIKE :search)
            GROUP BY l.name, l.url, l.color
        ')
            ->setParameters([
                "search" => $search,
            ]);
        $this->totalFiltered = count($query->getArrayResult());

        $select = '';
        foreach ($columns as $row) {
            if ($row == 'image') {
                $select .= 'i.fileName';
            } else {
                $select .= 'l.' . $row;
            }
            $select .= next($columns) ? ", " : "";
        }

        $query = $entityManager->createQuery(
            'SELECT ' . $select . '
            FROM App\Entity\Links l 
            JOIN l.imageId i
            WHERE l.xPrivate = 0
            AND (l.name LIKE :search
            OR l.url LIKE :search)
            GROUP BY l.name, l.url, l.color
            ORDER BY l.' . $column . ' ' . $type . '
        ')
            ->setMaxResults($lenght)
            ->setFirstResult($start)
            ->setParameters([
                "search" => $search,
            ]);
        $this->arrayResults = $query->getArrayResult();
    }

    public function total(): ?int
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT count(l)
            FROM App\Entity\Links l
            JOIN l.imageId i
            WHERE l.xPrivate = 0
            GROUP BY l.name, l.url, l.color
            '
        );

        $query->execute();
        return count($query->getArrayResult());
    }

    public function totalFiltered(): int
    {
        return $this->totalFiltered;
    }

    public function arrayResults(): array
    {
        return $this->arrayResults;
    }
    // /**
    //  * @return Links[] Returns an array of Links objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Links
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
