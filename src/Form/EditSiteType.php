<?php

namespace App\Form;

use App\Entity\Links;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditSiteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /**
         * @var Links $links
         */
        $links = $options['data']['links'];
        $builder
            ->add('name', TextType::class, [
                'label' => 'Name',
                'data' => $links->getName(),
                'attr' => [
                    'class' => 'form-control mb-3',
                ],
            ])
            ->add('url', UrlType::class, [
                'label' => 'URL',
                'data' => $links->getUrl(),
                'attr' => [
                    'class' => 'form-control mb-3',
                ],
            ])
            ->add('color', ColorType::class, [
                'label' => 'Color',
                'data' => $links->getColor(),
                'attr' => [
                    'class' => 'form-control mb-3',
                ],
            ])
            ->add('image', FileType::class, [
                'mapped' => false,
                'label' => 'Image',
                'required' => false,
                'attr' => [
                    'class' => 'form-control mb-3',
                ],
            ])
            ->add('xPrivate', CheckboxType::class, [
                'label' => 'Private link',
                'required' =>false,
                'data' => $links->getXPrivate(),
                'attr' => [
                    'class' => 'ml-1',
                ],
            ])
            ->add('xHidden', CheckboxType::class, [
                'label' => 'Hide',
                'required' =>false,
                'data' => $links->getXHidden(),
                'attr' => [
                    'class' => 'ml-1',
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Save',
                'attr' => [
                    'class' => 'btn btn-primary float-right',
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr' => [
                'onsubmit' => 'showLoader()',
            ]
        ]);
    }
}
