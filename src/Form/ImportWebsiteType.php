<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class ImportWebsiteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('site_file', FileType::class, [
                'label' => false,
//                'constraints' => [
//                    new File([
//                        'mimeTypes' => [
//                            'text/hpweb',
//                        ],
//                        'mimeTypesMessage' => 'Please upload a valid HomePage web file',
//                    ])
//                ],
                'attr' => [
                    'class' => 'form-control mb-3',
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Upload',
                'attr' => [
                    'class' => 'btn btn-primary float-right',
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([

            'attr' => [
                'onsubmit' => 'showLoader()',
            ]
        ]);
    }
}
