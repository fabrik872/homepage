<?php

namespace App\Controller;

use App\Services\AppServices;
use App\Services\DropboxServices;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/resources", name="resources.")
 * @Security("has_role('ROLE_USER')")
 */
class ResourcesSettingsController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(AppServices $appServices, DropboxServices $dropbox)
    {
        $dropboxOk = false;
        if($this->getUser()->getDropboxToken() == null) {
            $dropbox = null;
        } else{
            $dropboxOk = $dropbox->startDropbox($this->getUser());
        }

        $cacheSize = $appServices->folderSize('../var/cache');
        $imageCacheSize = $appServices->folderSize('../public/cache');
        $imagesSize = $appServices->folderSize('../public/uploads');
        $databaseSize = $this->file('../var/data.db');

        return $this->render('resources_settings/index.html.twig', [
            'databaseSize' => $databaseSize->getFile()->getSize(),
            'cacheSize' => $cacheSize,
            'imageCacheSize' => $imageCacheSize,
            'imagesSize' => $imagesSize,
            'appVersion' => $appServices->getVersion(),
            'dropboxOk' => $dropboxOk,
            'dropbox' => $dropbox,
        ]);
    }

    /**
     * @Route("/clear", name="clear")
     */
    public function clear(Request $request, AppServices $appServices)
    {

        if ($this->isCsrfTokenValid('clear_cache', $request->get('csfr_token'))) {
            if ($request->get("clear_web")) {
                exec("php ../bin/console cache:clear");
                $this->addFlash('success', 'Web cache cleared');
            } elseif ($request->get("clear_img")) {
                $appServices->rmdir('../public/cache');
                $this->addFlash('success', 'Image cache cleared');
            }
        }
        return $this->redirectToRoute('resources.index');
    }

    /**
     * @Route("/update", name="update")
     */
    public function update(Request $request, AppServices $appServices)
    {

        if ($request->get("update") && $this->isCsrfTokenValid('update', $request->get('csfr_token'))) {
            exec("git checkout -- .");
            $pull = exec("git pull https://fabrik872@bitbucket.org/fabrik872/homepage.git master");
            $composer = exec("composer install");
            exec("php ../bin/console cache:clear");
            exec("php ../bin/console doctrine:migrations:migrate -q");
            $this->addFlash('success', $pull . $composer);
        }
        return $this->redirectToRoute('resources.index');
    }
}
