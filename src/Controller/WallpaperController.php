<?php

namespace App\Controller;

use App\Entity\Wallpaper;
use App\Form\WallpaperType;
use App\Services\AppServices;
use App\Services\DropboxServices;
use App\Services\ImageSaver;
use Gregwar\Image\Image;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/wallpaper", name="wallpaper.")
 * @Security("has_role('ROLE_USER')")
 */
class WallpaperController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(Request $request, ImageSaver $imageSaver, DropboxServices $services)
    {
        $form = $this->createForm(WallpaperType::class);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {

            $services->autoPull($this->getUser());
            $formData = $form->getData()['images'];
            foreach ($formData as $uploadedFile) {
                $wallpaper = new Wallpaper();
                $wallpaper->setUserId($this->getUser());
                $imageSaver->saveImage($uploadedFile);
                $wallpaper->setImageId($imageSaver->getImage());
                $em->persist($wallpaper);
                $this->addFlash('success', 'Wallpaper ' . $wallpaper->getImageId()->getOriginalName() . ' added');
            }
            $em->flush();
            $services->autoPush($this->getUser());
            return $this->redirectToRoute('wallpaper.index');
        }
        $wallpapers = $em->getRepository(Wallpaper::class)->findBy(['userId' => $this->getUser()]);

        return $this->render('wallpaper/index.html.twig', [
            'form' => $form->createView(),
            'wallpapers' => $wallpapers,
        ]);
    }

    /**
     * @Route("/ajax/datatables", name="datatables", methods={"POST", "GET"})
     */
    public function datatables(Request $request, AppServices $appServices): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->getRepository(Wallpaper::class)->createQueryBuilder('w');
        $qb->join('w.imageId', 'i');
        $qb->where("w.userId = :user");
        $qb->setParameter('user', $this->getUser());

        $total = $qb->select('count(w)')->getQuery()->getSingleScalarResult();

        $qb->andWhere("i.originalName LIKE :search");
        $qb->setParameter('search', '%' . $request->get('search')['value'] . '%');

        $totalFiltered = $qb->select('count(w)')->getQuery()->getSingleScalarResult();

        $qb->select('w.id');
        $qb->addSelect('i.fileName as image');
        $qb->addSelect('i.date as date');
        if ($request->get('length')) {
            $qb->setFirstResult($request->get('start'));
            $qb->setMaxResults($request->get('length'));
        }
        $qb->orderBy('i.date', $request->get('order')[0]['dir']);

        $results = ($qb->getQuery()->getResult());

        foreach ($results as &$result) {
            $image = new Image($appServices->imageLink($result['image']));
            $result['image'] = $image->resize(null, 100)->guess();
            $result['date'] = $result['date']->format('d. m. y - H:i:s');
        }

        $json = new \stdClass();
        $json->draw = $request->get('draw');
        $json->recordsTotal = $total;
        $json->recordsFiltered = $totalFiltered;
        $json->data = $results;

        return new JsonResponse($json, 200);
    }

    /**
     * @Route("/delete", name="delete", methods={"POST"})
     */
    public function delete(Request $request, ImageSaver $imageSaver, DropboxServices $services)
    {
        $submittedToken = $request->get('token');
        $em = $this->getDoctrine()->getManager();
        if ($this->isCsrfTokenValid('delete-wallpaper', $submittedToken)) {
            $services->autoPull($this->getUser());
            if ($request->get('id')) {
                foreach ($request->get('id') as $wallpaperId) {
                    $wallpaper = $em->getRepository(Wallpaper::class)->find($wallpaperId);
                    $this->addFlash("success", "Wallpaper " . $wallpaper->getImageId()->getOriginalName() . " removed");
                    $imageSaver->loadImage($wallpaper->getImageId());
                    $imageSaver->delete();
                    $em->remove($wallpaper);
                }
                $em->flush();

                $services->autoPush($this->getUser());
            } else {
                $this->addFlash('warning', 'No wallpaper selected');
            }
            return $this->redirectToRoute('wallpaper.index');
        }
        return $this->createNotFoundException('The form must be filled and CSRF token must be valid.');
    }
}
