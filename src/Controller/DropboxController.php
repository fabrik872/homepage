<?php

namespace App\Controller;

use App\Entity\Images;
use App\Entity\User;
use App\Services\DropboxServices;
use League\Flysystem\Filesystem;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Srmklive\Dropbox\Adapter\DropboxAdapter;
use Srmklive\Dropbox\Client\DropboxClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/dropbox", name="dropbox.")
 * @Security("has_role('ROLE_USER')")
 */
class DropboxController extends AbstractController
{
    private $parameters;

    public function __construct(ParameterBagInterface $bag)
    {
        $this->parameters = $bag;
    }

    /**
     * @Route("/new_save", name="newSave", methods={"POST"})
     */
    public function newSave(Request $request, DropboxServices $dropbox)
    {
        if ($this->isCsrfTokenValid('newSave', $request->get('csfr_token'))) {
            try {
                $dropbox->startDropbox($this->getUser());
                $user = $this->getUser();
                $user->setBackupName($request->get("backupName"));
                $this->em()->persist($user);
                $this->em()->flush();
                $dropbox->push();
            } catch (\Exception $e) {
                $this->addFlash('danger', 'Error: ' . $e->getMessage());
                return $this->redirectToRoute('resources.index');
            }
            $this->addFlash('success', 'Backup ' . $user->getBackupName() . ' created');
            return $this->redirectToRoute('resources.index');
        }
        return new NotFoundHttpException('Page not found');
    }

    /**
     * @Route("/activate_backup", name="activateBackup", methods={"POST"})
     */
    public function activateBackup(Request $request)
    {
        if ($this->isCsrfTokenValid('activateBackup', $request->request->get('csfr_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            /** @var $user User */
            $user = $this->getUser();
            $user->setBackupName($request->get('backup'));
            $entityManager->persist($user);
            $entityManager->flush();
            $this->addFlash('success', 'Dropbox backup ' . $user->getBackupName() . ' activated');
        }

        return $this->redirectToRoute('resources.index');
    }

    /**
     * @Route("/pull_from_backup", name="pull", methods={"POST"})
     */
    public function pull(Request $request, DropboxServices $dropbox)
    {
        if ($this->isCsrfTokenValid('dropboxPull', $request->request->get('csfr_token'))) {
            try {
                $dropbox->startDropbox($this->getUser());
                $report = $dropbox->pull();
            } catch (\Exception $e) {
                $this->addFlash('danger', 'Error: ' . $e->getMessage());
                return $this->redirectToRoute('homepage');
            }
        }
        $this->addFlash('success', $report['downloaded'] . ' files downloaded and ' . $report['removed'] . ' removed');

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/push_to_backup", name="push", methods={"POST"})
     */
    public function push(Request $request, DropboxServices $dropbox)
    {
        if ($this->isCsrfTokenValid('dropboxPush', $request->request->get('csfr_token'))) {
            /**
             * @var $user User
             */
            try {
                $dropbox->startDropbox($this->getUser());
                $report = $dropbox->push();
            } catch (\Exception $e) {
                $this->addFlash('danger', 'Error: ' . $e->getMessage());
                return $this->redirectToRoute('homepage');
            }
            $this->addFlash('success', $report['uploaded'] . ' files uploaded and ' . $report['removed'] . ' removed');

            return $this->redirectToRoute('homepage');
        }
        return new NotFoundHttpException('Page not found');
    }

    /**
     * @Route("/new_token", name="newToken", methods={"POST"})
     */
    public function newToken(Request $request)
    {
        if ($this->isCsrfTokenValid('newToken', $request->request->get('csfr_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            /** @var $user User */
            $user = $this->getUser();
            $user->setDropboxToken($request->get('token'));
            $entityManager->persist($user);
            $entityManager->flush();
            $this->addFlash('success', 'Dropbox token created');
        }

        return $this->redirectToRoute('resources.index');
    }

    /**
     * @Route("/delete_token", name="deleteToken", methods={"DELETE"})
     */
    public function deleteToken(Request $request)
    {
        if ($this->isCsrfTokenValid('deleteToken', $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            /** @var $user User */
            $user = $this->getUser();
            $user->setDropboxToken('');
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('success', 'Dropbox token deleted');
        }

        return $this->redirectToRoute('resources.index');
    }

    /**
     * @Route("/autobackup", name="autoBackup")
     */
    public function autoBackup(Request $request, DropboxServices $services)
    {
        if ($this->isCsrfTokenValid('autoBackup', $request->get('_token'))) {
            /** @var $user User */
            $user = $this->getUser();
            $user->setAutosync((bool)$request->get('autoBackupActive'));
            $this->em()->persist($user);
            $this->em()->flush();
            $services->autoPush($this->getUser());

            if ((bool)$request->get('autoBackupActive')) {
                $this->addFlash('success', 'AutoBackups enabled');
            } else {
                $this->addFlash('success', 'AutoBackups disabled');
            }
        }
        return $this->redirectToRoute('resources.index');
    }

    private function em()
    {
        return $this->getDoctrine()->getManager();
    }
}
