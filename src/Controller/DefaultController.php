<?php

namespace App\Controller;

use App\Entity\Links;
use App\Form\AddSiteType;
use App\Form\EditSiteType;
use App\Services\DropboxServices;
use App\Services\ImageSaver;
use App\Services\OpenGraphExtractor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Security("has_role('ROLE_USER')")
 */
class DefaultController extends AbstractController
{
    /**
     * @Route("/{hidden}", name="homepage", defaults={"hidden"=null}, requirements={
     * "hidden": "show_hidden|null",
     * })
     */
    public function index(Request $request, ImageSaver $imageSaver, OpenGraphExtractor $og, $hidden, DropboxServices $services)
    {
        $form = $this->createForm(AddSiteType::class);
        $form->handleRequest($request);
        $linksRepo = $this->getDoctrine()->getManager()->getRepository(Links::class);

        if ($form->isSubmitted() && $form->isValid()) {
            $services->autoPull($this->getUser());
            /**
             * @var Links $link
             */
            $link = $form->getData();
            $link->setName($link->getUrl());
            try {
                $og->setUrl($link->getUrl());
                $link->setName($og->getUrlBase());
                $link = $og->fillLink($link);
            } catch (\Exception $e) {
                $this->addFlash('warning', 'Exception: ' . $e->getMessage());
            }
            $link->setUserId($this->getUser());
            $link->setXOrder($linksRepo->getLast($this->getUser()));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($link);
            $entityManager->flush();
            $this->addFlash('success', 'Website ' . $link->getName() . ' created');
            $services->autoPush($this->getUser());
            return $this->redirectToRoute('homepage');
        }

        $links = $linksRepo->getLinksByUser($this->getUser(), null, null, ($hidden == 'show_hidden'));
        $linksWForm = [];
        foreach ($links as $key => $link) {
            $entityManager = $this->getDoctrine()->getManager();
            $linksWForm[$key]['data'] = $link;
            $editForm = $this->createForm(EditSiteType::class, ['links' => $link]);
            $editForm->add('id', HiddenType::class, [
                'mapped' => false,
                'data' => $link->getId(),
            ]);
            $editForm->handleRequest($request);

            if ($editForm->isSubmitted() && $editForm->isValid()) {
                $services->autoPull($this->getUser());
                /**
                 * @var Links $link
                 */
                $link = $entityManager->getRepository(Links::class)->find($editForm->get('id')->getViewData());
                $link->setName($editForm->get('name')->getViewData());
                $link->setUrl($editForm->get('url')->getViewData());
                $link->setColor($editForm->get('color')->getViewData());
                $link->setXPrivate((bool)$editForm->get('xPrivate')->getViewData());
                $link->setXHidden((bool)$editForm->get('xHidden')->getViewData());
                if ($editForm->get('image')->getViewData()) {
                    $imageSaver->saveImage($editForm->get('image')->getViewData());
                    $link->setImageId($imageSaver->getImage());
                }
                $entityManager->persist($link);
                $entityManager->flush();
                $this->addFlash('success', 'Website ' . $link->getName() . ' updated');
                $services->autoPush($this->getUser());
                return $this->redirectToRoute('homepage');
            }

            $linksWForm[$key]['form'] = $editForm->createView();
        }

        return $this->render('default/index.html.twig', [
            'form' => $form->createView(),
            'links' => $linksWForm,
        ]);
    }

    /**
     * @Route("/ajax/sort", name="sort_links")
     */
    public function sort(Request $request, DropboxServices $services)
    {
        $items = $request->get('item');
        $em = $this->getDoctrine()->getManager();

        $services->autoPull($this->getUser());
        foreach ($items as $key => $itemId) {
            $link = $em->getRepository(Links::class)->find($itemId);

            $link->setXOrder($key);
            $em->persist($link);
        }

        $services->autoPush($this->getUser());
        $em->flush();

        return new Response(json_encode($items), 200);
    }

    /**
     * @Route("/sort/{sort}/{dir}", name="sort_all", requirements={
     * "sort": "name|date|usage",
     * "dir": "asc|desc"
     * })
     */
    public function sortAll($sort, $dir, DropboxServices $services)
    {
        $services->autoPull($this->getUser());
        $em = $this->getDoctrine()->getManager();
        if ($sort == 'usage') {
            $links = $em->getRepository(Links::class)->getLinksByUser($this->getUser(), 'usages', $dir);
        } else {
            $links = $em->getRepository(Links::class)->getLinksByUser($this->getUser(), 'l.' . $sort, $dir);
        }

        $i = 0;
        foreach ($links as $key => $link) {
            /**
             * @var $link Links
             */
            $link->setXOrder($i++);
            $em->persist($link);
        }

        $em->flush();
        $this->addFlash('success', 'Links is sorted by ' . $sort);

        $services->autoPush($this->getUser());
        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/delete/{id}", name="link_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Links $links, ImageSaver $imageSaver, DropboxServices $services): Response
    {
        if ($this->isCsrfTokenValid('delete' . $links->getId(), $request->request->get('_token'))) {
            $services->autoPull($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            if ($links->getImageId()) {
                $imageSaver->loadImage($links->getImageId());
                $imageSaver->delete();
            }
            $entityManager->remove($links);
            $entityManager->flush();

            $this->addFlash('success', 'Item ' . $links->getName() . ' deleted');
            $services->autoPush($this->getUser());
        }

        return $this->redirectToRoute('homepage');
    }
}
