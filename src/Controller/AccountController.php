<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ProfilePicType;
use App\Services\DropboxServices;
use App\Services\ImageSaver;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AccountController
 * @package App\Controller
 * @Route("/account", name="account.")
 */
class AccountController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(Request $request, ImageSaver $imageSaver, DropboxServices $services)
    {
        $form = $this->createForm(ProfilePicType::class);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {

            $services->autoPull($this->getUser());
            $profilePic = $form->getData()['profilePic'];
            /**@var $user User */
            $user = $this->getUser();
            $imageSaver->saveImage($profilePic);
            $user->setProfilePicture($imageSaver->getImage());
            $em->persist($user);
            $this->addFlash('success', 'Profile picture added');
            $em->flush();
            $services->autoPush($this->getUser());
            return $this->redirectToRoute('account.index');
        }

        return $this->render('account/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/change_pw", name="changePw")
     */
    public function changePw(Request $request, DropboxServices $services)
    {
        /**
         * @var $user User
         */

        if ($this->isCsrfTokenValid('change_pw', $request->get('csfr_token'))) {
            $services->autoPull($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser();

            if (password_verify($request->get('oldPassword'), $user->getPassword())) {
                $user->setPassword(password_hash($request->get('newPassword'), PASSWORD_DEFAULT));
                $em->persist($user);
                $em->flush();

                $this->addFlash('success', 'Password succesfully changed');
                $services->autoPush($this->getUser());
            } else {
                $this->addFlash('danger', 'Old password not match');
            }
        }
        return $this->redirectToRoute('account.index');
    }

    /**
     * @Route("/delete_profile_pic", name="deleteProfilePic", methods={"POST"})
     */
    public function delete(Request $request, DropboxServices $services, ImageSaver $imageSaver)
    {
        $submittedToken = $request->get('token');
        $em = $this->getDoctrine()->getManager();
        if ($this->isCsrfTokenValid('deleteProfilePicture', $submittedToken)) {
            $services->autoPull($this->getUser());
            /**@var $user User */
            $user = $this->getUser();
            $image = $user->getProfilePicture();
            $user->setProfilePicture(null);
            $em->flush();
            $imageSaver->loadImage($image);
            $imageSaver->delete();
            $this->addFlash('success', 'Profile picture deleted');
            $services->autoPush($this->getUser());
            return $this->redirectToRoute('account.index');
        }
        return $this->createNotFoundException('The form must be filled and CSRF token must be valid.');
    }
}
