<?php

namespace App\Controller;

use App\Entity\Links;
use App\Entity\LinksLog;
use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/statistics", name="statistics.")
 * @Security("has_role('ROLE_USER')")
 */
class StatisticsController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();

        $usageChart = $em->getRepository(LinksLog::class)->getTopSites($this->getUser());
        $usageChartData[] = ['Website', 'Usage', ["role" => "style"]];
        $totalUsages = $em->getRepository(LinksLog::class)->countAllSitesUsages($this->getUser());

        foreach ($usageChart as $value) {
            /**
             * @var Links $links
             */
            $links = $value['link'];
            $usageChartData[] = [$links->getName(), round($value['usages'] / $totalUsages * 100, 2), 'color: ' . $links->getColor()];
        }
        $noData = true;
        if ($usageChart) {
            $noData = false;
        }

        return $this->render('statistics/index.html.twig', [
            'noData' => $noData,
            'siteUsageData' => json_encode($usageChartData),
        ]);
    }

    /**
     * @Route("/time_usage_data", name="timeUsageData", methods={"GET"})
     */
    public function timeUsageData(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $timeChartData[] = ['day', 'visits'];
        $today = new \DateTime('now');
        for ($i = 1; $i <= 31; $i++) {
            if ((int)$today->format('d') == $i && $request->get('month') == 0) {
                $timeChartData[] = ["Today", 0];
            } else {
                $timeChartData[] = [(string)sprintf('%02s', $i), 0];
            }
        }
        $timeChart = $em->getRepository(LinksLog::class)->getAllSitesAllTime($this->getUser(), $request->get('month'));
        foreach ($timeChart as $value) {
            /**
             * @var \DateTime $date
             */
            $date = $value['date'];
            $timeChartData[(int)$date->format("d")][1] = (int)$value['usages'];
        }

        return $this->json($timeChartData);
    }

    /**
     * @Route("/delete/history", name="delete", methods={"DELETE"})
     */
    public function delete(Request $request): Response
    {
        if ($this->isCsrfTokenValid('delete', $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->getRepository(LinksLog::class)->deleteAll($this->getUser());

            $this->addFlash('success', 'History deleted');
        }

        return $this->redirectToRoute('statistics.index');
    }
}
