<?php

namespace App\Controller;

use App\Entity\Links;
use App\Form\ImportWebsiteType;
use App\Services\AppServices;
use App\Services\ExportSercives;
use App\Services\ImageSaver;
use App\Services\UploadedBase64EncodedFile;
use Nzo\UrlEncryptorBundle\Annotations\ParamDecryptor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mime\Encoder\Base64ContentEncoder;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/export", name="export.")
 * @Security("has_role('ROLE_USER')")
 */
class ExportController extends AbstractController
{
    private $appServices;

    public function __construct(AppServices $appServices)
    {
        $this->appServices = $appServices;
    }

    /**
     * @Route("/import", name="import")
     */
    public function import(Request $request, ExportSercives $exportSercives)
    {
        $form = $this->createForm(ImportWebsiteType::class);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                /**
                 * @var UploadedFile $uploadedFile
                 */
                $uploadedFile = $form->getData()['site_file'];
                $data = json_decode(fread(fopen($uploadedFile->getRealPath(), "r"), filesize($uploadedFile->getRealPath())));

                foreach ($data as $item) {
                    $link = $exportSercives->importLink($item, $this->getUser());
                    $em->persist($link);

                    $this->addFlash('success', 'Website ' . $link->getName() . ' imported');
                }
                $em->flush();
            } catch (\Exception $exception) {
                $this->addFlash('danger', 'Error: ' . $exception->getMessage());
                return $this->redirectToRoute('export.import');
            }

            return $this->redirectToRoute('homepage');
        }

        return $this->render('export/import.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/all", name="generateAll")
     */
    public function generateAll(ExportSercives $exportSercives)
    {
        $response = new JsonResponse();

        //set headers
        $response->headers->set('Content-Disposition', 'attachment;filename="' . $this->getUser()->getEmail() . '.json');
        $response->headers->set('Content-Type', 'application/json');

        $links = $this->getDoctrine()->getManager()->getRepository(Links::class)->findBy(['userId' => $this->getUser()],
            ['xOrder' => 'ASC']);
        $objects = [];
        foreach ($links as $link) {
            $objects[] = $exportSercives->exportLink($link);
        }

        $response->setContent(json_encode($objects));

        return $response;
    }

    /**
     * @Route("/{id}", name="generate")
     * @ParamDecryptor(params={"id", "links"})
     */
    public function generate(Links $links, ExportSercives $exportSercives)
    {
        $response = new JsonResponse();

        //set headers
        $response->headers->set('Content-Disposition', 'attachment;filename="' . mb_convert_case($links->getName(), MB_CASE_LOWER, "UTF-8") . '.json');
        $response->headers->set('Content-Type', 'application/json');

        $response->setContent(json_encode([$exportSercives->exportLink($links)]));

        return $response;
    }
}
