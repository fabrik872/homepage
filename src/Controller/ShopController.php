<?php

namespace App\Controller;

use App\Entity\Links;
use App\Services\AppServices;
use App\Services\DropboxServices;
use Gregwar\Image\Image;
use Nzo\UrlEncryptorBundle\Annotations\ParamDecryptor;
use Nzo\UrlEncryptorBundle\UrlEncryptor\UrlEncryptor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/shop", name="shop.")
 * @Security("has_role('ROLE_USER')")
 */
class ShopController extends AbstractController
{
    private $columns;

    public function __construct()
    {
        $this->columns = ['name', 'url', 'image'];
    }

    /**
     * @Route("/", name="index")
     */
    public function index()
    {

        return $this->render('shop/index.html.twig', [
            'columns' => $this->columns,
        ]);
    }

    /**
     * @Route("/ajax/datatables", name="datatables", methods={"POST"})
     */
    public function datatables(Request $request, UrlEncryptor $urlEncryptor, AppServices $appServices): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $links = $em->getRepository(Links::class);
        $column = $this->columns[$request->request->get('order')[0]['column']];

        $links->select(
            $request->request->get('start'),
            $request->request->get('length'),
            $column,
            $request->request->get('order')[0]['dir'],
            array_merge($this->columns, ['id', 'color']),
            $request->request->get('search')['value']
        );

        $data = $links->arrayResults();

        foreach ($data as $key => $row) {
            $image = new Image($appServices->imageLink($row['fileName']));
            $data[$key]["image"]["file"] = '/' . $image->resize(120)->guess();
            $data[$key]["image"]["color"] = $row['color'];
            $data[$key]["actions"]["download"] = $this->generateUrl("export.generate", ["id" => $urlEncryptor->encrypt($row["id"])]);
            $data[$key]["actions"]["add"] = $this->generateUrl("shop.add", ["id" => $urlEncryptor->encrypt($row["id"])]);
        }

        $json = new \stdClass();
        $json->draw = $request->request->get('draw');
        $json->recordsTotal = $links->total();
        $json->recordsFiltered = $links->totalFiltered();
        $json->data = $data;

        return new JsonResponse($json, 200);
    }

    /**
     * @Route("/add/{id}", name="add")
     * @ParamDecryptor(params={"id"})
     */
    public function add(Links $links, DropboxServices $services)
    {
        $services->autoPull($this->getUser());
        $em = $this->getDoctrine()->getManager();
        $newLinks = clone $links;
        $newLinks->setImageId(clone $links->getImageId());
        $newLinks->setXOrder($em->getRepository(Links::class)->getLast($this->getUser()));
        $em->persist($newLinks);
        $em->flush();
        $this->addFlash('success', 'Website ' . $newLinks->getName() . ' created');
        $services->autoPush($this->getUser());

        return $this->redirectToRoute('shop.index');
    }
}
