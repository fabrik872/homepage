<?php

namespace App\Controller;

use App\Entity\Links;
use App\Entity\LinksLog;
use App\Services\AppServices;
use Gregwar\Image\Image;
use Nzo\UrlEncryptorBundle\Annotations\ParamDecryptor;
use Nzo\UrlEncryptorBundle\UrlEncryptor\UrlEncryptor;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/links", name="links.")
 * @Security("has_role('ROLE_USER')")
 */
class LinksController extends AbstractController
{
    /**
     * @Route("/quickSearch", name="quickSearch", methods={"POST"})
     */
    public function quickSearch(Request $request, UrlEncryptor $encryptor, AppServices $appServices): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository(Links::class)->createQueryBuilder('l');
        $qb->select('l')
            ->where("l.userId = :user")
            ->andWhere("l.name LIKE :search")
            ->orWhere("l.url LIKE :search")
            ->orWhere("l.date LIKE :search")
            ->setParameter('user', $this->getUser())
            ->setParameter('search', '%' . $request->get('search') . '%');

        $output = [];
        foreach ($qb->getQuery()->getResult() as $link) {
            /**
             * @var Links $link
             */
            $linkData = new \stdClass();
            $image = new Image($appServices->imageLink($link->getImageId()));
            $linkData->name = $link->getName();
            $linkData->url = $this->generateUrl('links.index', ['id' => $encryptor->encrypt($link->getId())]);
            $linkData->img = '/' . $image->resize(50)->guess();
            $linkData->color = $link->getColor();
            $linkData->alt = ($link->getImageId()) ? $link->getImageId()->getOriginalName() : '';

            $output[] = $linkData;
        }
        return new JsonResponse($output);
    }

    /**
     * @Route("/{id}", name="index")
     * @ParamDecryptor(params={"id", "links"})
     */
    public function index(Links $links)
    {
        $em = $this->getDoctrine()->getManager();
        $linksLog = new LinksLog();
        $linksLog->setLinksId($links);
        $em->persist($linksLog);
        $em->flush();

        return $this->redirect($links->getUrl());
    }
}
