<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191204122535 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_772221939CA565AC');
        $this->addSql('CREATE TEMPORARY TABLE __temp__links_log AS SELECT id, links_id_id, date FROM links_log');
        $this->addSql('DROP TABLE links_log');
        $this->addSql('CREATE TABLE links_log (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, links_id_id INTEGER NOT NULL, date DATETIME NOT NULL, CONSTRAINT FK_772221939CA565AC FOREIGN KEY (links_id_id) REFERENCES links (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO links_log (id, links_id_id, date) SELECT id, links_id_id, date FROM __temp__links_log');
        $this->addSql('DROP TABLE __temp__links_log');
        $this->addSql('CREATE INDEX IDX_772221939CA565AC ON links_log (links_id_id)');
        $this->addSql('DROP INDEX IDX_D592642C9D86650F');
        $this->addSql('DROP INDEX UNIQ_D592642C68011AFE');
        $this->addSql('CREATE TEMPORARY TABLE __temp__wallpaper AS SELECT id, image_id_id, user_id_id FROM wallpaper');
        $this->addSql('DROP TABLE wallpaper');
        $this->addSql('CREATE TABLE wallpaper (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, image_id_id INTEGER NOT NULL, user_id_id INTEGER NOT NULL, CONSTRAINT FK_D592642C68011AFE FOREIGN KEY (image_id_id) REFERENCES images (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_D592642C9D86650F FOREIGN KEY (user_id_id) REFERENCES user (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO wallpaper (id, image_id_id, user_id_id) SELECT id, image_id_id, user_id_id FROM __temp__wallpaper');
        $this->addSql('DROP TABLE __temp__wallpaper');
        $this->addSql('CREATE INDEX IDX_D592642C9D86650F ON wallpaper (user_id_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D592642C68011AFE ON wallpaper (image_id_id)');
        $this->addSql('DROP INDEX IDX_D182A1189D86650F');
        $this->addSql('DROP INDEX UNIQ_D182A11868011AFE');
        $this->addSql('CREATE TEMPORARY TABLE __temp__links AS SELECT id, image_id_id, user_id_id, name, url, date, x_order, color, x_private, x_hidden FROM links');
        $this->addSql('DROP TABLE links');
        $this->addSql('CREATE TABLE links (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, image_id_id INTEGER DEFAULT NULL, user_id_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL COLLATE BINARY, url CLOB NOT NULL COLLATE BINARY, date DATETIME NOT NULL, x_order INTEGER NOT NULL, color VARCHAR(8) DEFAULT NULL COLLATE BINARY, x_private BOOLEAN NOT NULL, x_hidden BOOLEAN NOT NULL, CONSTRAINT FK_D182A11868011AFE FOREIGN KEY (image_id_id) REFERENCES images (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_D182A1189D86650F FOREIGN KEY (user_id_id) REFERENCES user (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO links (id, image_id_id, user_id_id, name, url, date, x_order, color, x_private, x_hidden) SELECT id, image_id_id, user_id_id, name, url, date, x_order, color, x_private, x_hidden FROM __temp__links');
        $this->addSql('DROP TABLE __temp__links');
        $this->addSql('CREATE INDEX IDX_D182A1189D86650F ON links (user_id_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D182A11868011AFE ON links (image_id_id)');
        $this->addSql('DROP INDEX IDX_338BC3146205D0B8');
        $this->addSql('DROP INDEX IDX_338BC314C0DE588D');
        $this->addSql('CREATE TEMPORARY TABLE __temp__links_keywords AS SELECT links_id, keywords_id FROM links_keywords');
        $this->addSql('DROP TABLE links_keywords');
        $this->addSql('CREATE TABLE links_keywords (links_id INTEGER NOT NULL, keywords_id INTEGER NOT NULL, PRIMARY KEY(links_id, keywords_id), CONSTRAINT FK_338BC314C0DE588D FOREIGN KEY (links_id) REFERENCES links (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_338BC3146205D0B8 FOREIGN KEY (keywords_id) REFERENCES keywords (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO links_keywords (links_id, keywords_id) SELECT links_id, keywords_id FROM __temp__links_keywords');
        $this->addSql('DROP TABLE __temp__links_keywords');
        $this->addSql('CREATE INDEX IDX_338BC3146205D0B8 ON links_keywords (keywords_id)');
        $this->addSql('CREATE INDEX IDX_338BC314C0DE588D ON links_keywords (links_id)');
        $this->addSql('DROP INDEX UNIQ_8D93D649292E8AE2');
        $this->addSql('DROP INDEX UNIQ_8D93D649E7927C74');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user AS SELECT id, profile_picture_id, email, roles, password FROM user');
        $this->addSql('DROP TABLE user');
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, profile_picture_id INTEGER DEFAULT NULL, email VARCHAR(180) NOT NULL COLLATE BINARY, roles CLOB NOT NULL COLLATE BINARY --(DC2Type:json)
        , password VARCHAR(255) NOT NULL COLLATE BINARY, CONSTRAINT FK_8D93D649292E8AE2 FOREIGN KEY (profile_picture_id) REFERENCES images (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO user (id, profile_picture_id, email, roles, password) SELECT id, profile_picture_id, email, roles, password FROM __temp__user');
        $this->addSql('DROP TABLE __temp__user');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649292E8AE2 ON user (profile_picture_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON user (email)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_D182A11868011AFE');
        $this->addSql('DROP INDEX IDX_D182A1189D86650F');
        $this->addSql('CREATE TEMPORARY TABLE __temp__links AS SELECT id, image_id_id, user_id_id, name, url, date, x_order, color, x_private, x_hidden FROM links');
        $this->addSql('DROP TABLE links');
        $this->addSql('CREATE TABLE links (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, image_id_id INTEGER DEFAULT NULL, user_id_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL, url CLOB NOT NULL, date DATETIME NOT NULL, x_order INTEGER NOT NULL, color VARCHAR(8) DEFAULT NULL, x_private BOOLEAN NOT NULL, x_hidden BOOLEAN NOT NULL)');
        $this->addSql('INSERT INTO links (id, image_id_id, user_id_id, name, url, date, x_order, color, x_private, x_hidden) SELECT id, image_id_id, user_id_id, name, url, date, x_order, color, x_private, x_hidden FROM __temp__links');
        $this->addSql('DROP TABLE __temp__links');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D182A11868011AFE ON links (image_id_id)');
        $this->addSql('CREATE INDEX IDX_D182A1189D86650F ON links (user_id_id)');
        $this->addSql('DROP INDEX IDX_338BC314C0DE588D');
        $this->addSql('DROP INDEX IDX_338BC3146205D0B8');
        $this->addSql('CREATE TEMPORARY TABLE __temp__links_keywords AS SELECT links_id, keywords_id FROM links_keywords');
        $this->addSql('DROP TABLE links_keywords');
        $this->addSql('CREATE TABLE links_keywords (links_id INTEGER NOT NULL, keywords_id INTEGER NOT NULL, PRIMARY KEY(links_id, keywords_id))');
        $this->addSql('INSERT INTO links_keywords (links_id, keywords_id) SELECT links_id, keywords_id FROM __temp__links_keywords');
        $this->addSql('DROP TABLE __temp__links_keywords');
        $this->addSql('CREATE INDEX IDX_338BC314C0DE588D ON links_keywords (links_id)');
        $this->addSql('CREATE INDEX IDX_338BC3146205D0B8 ON links_keywords (keywords_id)');
        $this->addSql('DROP INDEX IDX_772221939CA565AC');
        $this->addSql('CREATE TEMPORARY TABLE __temp__links_log AS SELECT id, links_id_id, date FROM links_log');
        $this->addSql('DROP TABLE links_log');
        $this->addSql('CREATE TABLE links_log (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, links_id_id INTEGER NOT NULL, date DATETIME NOT NULL)');
        $this->addSql('INSERT INTO links_log (id, links_id_id, date) SELECT id, links_id_id, date FROM __temp__links_log');
        $this->addSql('DROP TABLE __temp__links_log');
        $this->addSql('CREATE INDEX IDX_772221939CA565AC ON links_log (links_id_id)');
        $this->addSql('DROP INDEX UNIQ_8D93D649E7927C74');
        $this->addSql('DROP INDEX UNIQ_8D93D649292E8AE2');
        $this->addSql('CREATE TEMPORARY TABLE __temp__user AS SELECT id, profile_picture_id, email, roles, password FROM user');
        $this->addSql('DROP TABLE user');
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, profile_picture_id INTEGER DEFAULT NULL, email VARCHAR(180) NOT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , password VARCHAR(255) NOT NULL)');
        $this->addSql('INSERT INTO user (id, profile_picture_id, email, roles, password) SELECT id, profile_picture_id, email, roles, password FROM __temp__user');
        $this->addSql('DROP TABLE __temp__user');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON user (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649292E8AE2 ON user (profile_picture_id)');
        $this->addSql('DROP INDEX UNIQ_D592642C68011AFE');
        $this->addSql('DROP INDEX IDX_D592642C9D86650F');
        $this->addSql('CREATE TEMPORARY TABLE __temp__wallpaper AS SELECT id, image_id_id, user_id_id FROM wallpaper');
        $this->addSql('DROP TABLE wallpaper');
        $this->addSql('CREATE TABLE wallpaper (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, image_id_id INTEGER NOT NULL, user_id_id INTEGER NOT NULL)');
        $this->addSql('INSERT INTO wallpaper (id, image_id_id, user_id_id) SELECT id, image_id_id, user_id_id FROM __temp__wallpaper');
        $this->addSql('DROP TABLE __temp__wallpaper');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D592642C68011AFE ON wallpaper (image_id_id)');
        $this->addSql('CREATE INDEX IDX_D592642C9D86650F ON wallpaper (user_id_id)');
    }
}
