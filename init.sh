#!/bin/bash
root="/var/www/html"
if [ ! -d "${root}/.git" ]; then
  git clone https://bitbucket@bitbucket.org/fabrik872/homepage.git $root
  composer install -d $root
  chown -R www-data:www-data $root
  ${root}/bin/console cache:warmup

  if [ ! -f "${root}/var/data.db" ]; then
    ${root}/bin/console doctrine:database:create
    ${root}/bin/console doctrine:schema:update --force
  fi
else
  git reset --hard origin/master
  composer install -d $root
  chown -R www-data:www-data $root
  ${root}/bin/console cache:warmup
fi

chmod 777 -R $root
/usr/sbin/apache2ctl -D FOREGROUND
